package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.Set;

public interface PostService {

    //create a post
    void createPost(String stringToken, Post post);

    //getting all posts
    Iterable<Post> getPosts();

    ResponseEntity updatePost(Long id, String stringToken, Post post);

    ResponseEntity deletePost(Long id, String stringToken);

    Set<Post> getUserPosts(String stringToken);

}
