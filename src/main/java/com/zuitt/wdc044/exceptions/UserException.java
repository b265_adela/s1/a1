package com.zuitt.wdc044.exceptions;


//This will hold an exception(error) messaged during the registration
public class UserException extends Exception {

    public UserException(String message){
        super(message);
    }
}