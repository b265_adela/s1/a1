package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//"@SpringBootApplication" this is called as "Annotations" mark.
//Annotations are used to provide supplemental information about the program
//There are used to manage and configure the behavior of the framework
@SpringBootApplication
//Tells the spring boot that this will handle endpoints for web request.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		//This method starts the whole Spring framework
		SpringApplication.run(Wdc044Application.class, args);
	}

	//This is used for mapping HTTP GET requests.
	@GetMapping("/hello")
	//"@RequestParam" is used to extract query parameters, form parameters and even files from the request
	//name = john; Hello John
	// To append the URL with a name parameter we do the following:
	//http:localhost:8080/hello?name=John
	// "?" means the start of the parameters followed by the "key-value" pairs
	public String hello(@RequestParam(value= "name", defaultValue = "World") String name) {
		return String.format("Hello %s", name);
	}

	@GetMapping("/greetings")
	public String greetings(@RequestParam(value= "greet", defaultValue = "World") String greet) {
		return String.format("Good evening, %s! Welcome to Batch 265", greet);
	}

	@GetMapping("/contactInfo")
	public String contactInfo(@RequestParam(value="name", defaultValue = "User") String name, @RequestParam(value="email", defaultValue = "user@mail.com") String email) {

		return String.format("Hello %s! Your email is %s.", name, email);
	}

	// Activity s01
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "user", defaultValue = "user") String user) {
		return String.format("hi %s!", user);
	}

	// Stretch goals
	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value = "name", defaultValue = "Juan") String name, @RequestParam(value = "age", defaultValue = "18") String age){
		return String.format("Hello %s! Your age is %s.", name, age);
	}


}

// Creating a spring boot project
// Go to https://start.spring.io/ to initialize a spring boot project
// Set up the following:
// Project: Maven
// Language: Java
// Spring Boot: 2.7.9 (or the most previous update)
// Group: com.nameOfThePackage
// Artifact & Name: projectName
// Packaging: War (for creating web apps)
// Java: 11 (jdk version)
// Under Dependencies: Click Add Dependencies > Spring Web
// Click Generate
// Extract the Zip file and open the file in intellij
// Look for the pom.xml and do the following:
// Right Click pom.xml > Look for Maven > Download Sources > Wait for the  download to be done.
// Right Click again pom.xml > Maven >  Reload Project > This should fix any errors on first time of opening the project.
// Note: the steps above should also be followed if new dependencies is added on the pom.xml

// command for running the application:
// ./mvnw spring-boot:run
