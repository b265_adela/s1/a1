package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.JwtRequest;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.repositories.UserRepository;
import com.zuitt.wdc044.services.PostService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @Autowired
    JwtToken jwtToken;

    // Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){

        postService.createPost(stringToken, post);

        return new ResponseEntity<>("Post create successfully", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(){

        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    //Edit a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postId, stringToken, post);
    }

    //deleting a post
    @RequestMapping(value ="/posts/{postId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken){
        return postService.deletePost(postId, stringToken);
    }

    @RequestMapping(value ="/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserPosts(@RequestHeader (value = "Authorization") String stringToken){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

}
